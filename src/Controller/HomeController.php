<?php

namespace App\Controller;

use App\Entity\Mot;
use App\Form\MotType;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use App\Repository\MotRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(MotRepository $motRepository): Response
    {
        $mots = $motRepository->findAll();
        $code = bin2hex(random_bytes((int) ceil(3)));

        return $this->render('home/index.html.twig', [
            'mots' => $mots,
            'code' => $code
        ]);
    }

    #[Route('/ajouter-un-mot', name: 'app_ajouter_mot')]
    public function ajouterMot(
        Request $request,
        EntityManagerInterface $em // Injection correcte de l'EntityManagerInterface
    ): Response {
        $mot = new Mot();
        $form = $this->createForm(MotType::class, $mot);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $mot->setCreatedAt(new DateTimeImmutable());
            $em->persist($mot);
            $em->flush();
            return $this->redirectToRoute('app_home');
        }

        // Afficher le formulaire dans la vue
        return $this->render('home/mot-ajouter.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
