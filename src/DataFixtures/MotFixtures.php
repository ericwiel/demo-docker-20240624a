<?php
// src/DataFixtures/MotFixtures.php

namespace App\DataFixtures;

use App\Entity\Mot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MotFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR'); // Utilisation de Faker pour générer des données en français

        for ($i = 0; $i < 15; $i++) {
            $mot = new Mot();
            $mot->setMot($faker->sentence) // Génère un mot aléatoire
                ->setCreatedAt(new \DateTimeImmutable()); // Génère une date aléatoire cette année

            $manager->persist($mot); // Persist l'entité pour l'ajouter à l'unité de travail
        }

        $manager->flush(); // Enregistre toutes les entités persistées dans la base de données
    }
}
