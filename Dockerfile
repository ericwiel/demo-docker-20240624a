# Étape 1: PHP avec les extensions requises pour Symfony
FROM php:8.2-apache AS php

# Installez les extensions PHP requises pour Symfony et le client MySQL
RUN apt-get update && apt-get install -y \
    libicu-dev \
    libzip-dev \
    zip \
    unzip \
    git \
    curl \
    gnupg \
    && docker-php-ext-install intl pdo pdo_mysql opcache zip

# Installez Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Installez la CLI Symfony
RUN curl -sS https://get.symfony.com/cli/installer | bash \
    && mv /root/.symfony*/bin/symfony /usr/local/bin/symfony

# Activez le module Apache pour la réécriture d'URL
RUN a2enmod rewrite

# Copiez la configuration Apache pour Symfony
COPY docker/apache/symfony.conf /etc/apache2/sites-available/000-default.conf

# Copiez les fichiers de l'application Symfony dans le répertoire d'Apache
COPY . /var/www/html/

# Changez le propriétaire des fichiers pour www-data (l'utilisateur Apache)
RUN chown -R www-data:www-data /var/www/html

# Exposez le port 80 pour Apache
EXPOSE 80

# Commande pour démarrer Apache
CMD ["apache2-foreground"]